const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const path = require('path')

const app = express();
const httpServer = require("http").createServer(app);
const port = 8081
const io = require("socket.io")(httpServer, {});

const crypto = require('crypto');

//  _    __
// | |  / /___ ___________
// | | / / __ `/ ___/ ___/
// | |/ / /_/ / /  (__  )
// |___/\__,_/_/  /____/
//
// This has to be refractored to the DB but i'm lazy for the time being
//

let rooms = {};
let completedRooms = {};

//     ___
//    /   |  ____  ____
//   / /| | / __ \/ __ \
//  / ___ |/ /_/ / /_/ /
// /_/  |_/ .___/ .___/
//       /_/   /_/

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
app.use(
  express.urlencoded({
    extended: true
  })
)
app.use(express.json())

app.use('/', express.static(path.join(__dirname, 'public')))

//     ____              __
//    / __ \____  __  __/ /____  _____
//   / /_/ / __ \/ / / / __/ _ \/ ___/
//  / _, _/ /_/ / /_/ / /_/  __(__  )
// /_/ |_|\____/\__,_/\__/\___/____/


app.post('/post-test', (req, res) => {
  const {
    survivors,
    killer,
    disabledPerks,
    perkCount
  } = req.body;

  createRoom({
    survivors,
    killer,
    disabledPerks,
    perkCount
  })

  res.sendStatus(200);

});

//    _____            __        __
//   / ___/____  _____/ /_____  / /_
//   \__ \/ __ \/ ___/ //_/ _ \/ __/
//  ___/ / /_/ / /__/ ,< /  __/ /_
// /____/\____/\___/_/|_|\___/\__/


io.on("connection", socket => {
  console.log(socket)
});

//     ______                 __  _
//    / ____/_  ______  _____/ /_(_)___  ____  _____
//   / /_  / / / / __ \/ ___/ __/ / __ \/ __ \/ ___/
//  / __/ / /_/ / / / / /__/ /_/ / /_/ / / / (__  )
// /_/    \__,_/_/ /_/\___/\__/_/\____/_/ /_/____/

function createRoom(settings) {

  const hashString = crypto.createHash('sha256').update(Date.now().toString()).digest('hex');

  rooms[hashString] = {
    shortHashString: hashString.substr(0, 16),
    settings
  };

  console.log(rooms[hashString])
  console.log(Object.values(rooms).length)

}

//    _____ __             __     _____
//   / ___// /_____ ______/ /_   / ___/___  ______   _____  _____
//   \__ \/ __/ __ `/ ___/ __/   \__ \/ _ \/ ___/ | / / _ \/ ___/
//  ___/ / /_/ /_/ / /  / /_    ___/ /  __/ /   | |/ /  __/ /
// /____/\__/\__,_/_/   \__/   /____/\___/_/    |___/\___/_/

httpServer.listen(port);

